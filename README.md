# K3d

## install k3d

```bash
brew install k3d
```

## deploy cluster, 1 control-plane 3 nodes

```bash
k3d cluster create --api-port 6550 -p "8888:80@loadbalancer" --agents 3
```

## configs

```bash
export KUBECONFIG="$(k3d kubeconfig write k3s-default)"
```

## create deployment

```bash
kubectl create deployment nginx --image=devopsjourney1/mywebapp:latest --replicas=3
```

## create service

```bash
kubectl create service clusterip nginx --tcp=80:80
```

## create file ingress.yaml

```yaml
# apiVersion: networking.k8s.io/v1beta1 # for k3s < v1.19
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx
  annotations:
    ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx
            port:
              number: 80
```

## apply ingress

```bash
kubectl apply -f ingress.yaml
```

## test

```bash
curl <IP/HOST>:8081
```

